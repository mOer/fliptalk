(function ($, undefined) {
    $.fn.flip = function (options) {
        return this.each(function(index) {
            var opts = $.extend({}, $.fn.flip.defaults, options),
            	$this = $(this),
            	$target = $(opts.target),
            	$front = $target.find('.front'),
            	$back = $target.find('.back');
            
            $this.click(function(e) {
            	e.preventDefault();
            	
            	$target.toggleClass('flipped');
            	
            	if ($target.hasClass('flipped')) {
            		setTimeout(function() {
            			$back.toggleClass('hide');
            			$front.toggleClass('hide');
            		}, 320); // 5000:1600
            		setTimeout(function() {
            			jsPlumb.ready(function() {
            				jsPlumb.endpointClass = "endpoint";
            				jsPlumb.connectorClass = "connector";
            				
            				var offsetY = 0, 
            				stateMachineConnectorToRight = {
        						connector:"StateMachine",
        						detachable:false,
        						paintStyle: {lineWidth: 2, strokeStyle:"#ED9E34"},
            					overlays:[ ["PlainArrow", {location:1, width:15, length:12} ]],
        						endpoint:"Blank",
        						anchors:[[1, 0.5, 0, 0, 0, offsetY - 10], "LeftMiddle"]
            				}, stateMachineConnectorToLeft = {
        						connector:"StateMachine",
        						detachable:false,
        						paintStyle: {lineWidth: 2, strokeStyle:"#3175C0"},
            					overlays:[ ["PlainArrow", {location:1, width:15, length:12} ]],
        						endpoint:"Blank",
        						anchors:["LeftMiddle", "RightMiddle"]
                			};
            				
            				console.log(stateMachineConnectorToRight);
            				
            				jsPlumb.connect({
            					source: 'context0agree0',
            					target: 'context0disagree0',
            					container: $('#flipTalkComment .comments').first()
            				}, stateMachineConnectorToRight);
            				
            				offsetY = offsetY + 10;
            				stateMachineConnectorToRight.anchors[0][5] = offsetY;
            				console.log(stateMachineConnectorToRight);
            				
            				jsPlumb.connect({
            					source: 'context0agree0',
            					target: 'context0disagree1',
            					container: $('#flipTalkComment .comments').first()
            				}, stateMachineConnectorToRight);
            				
            				jsPlumb.connect({
            					source: 'context0disagree0',
            					target: 'context0agree1',
            					container: $('#flipTalkComment .comments').first()
            				}, stateMachineConnectorToLeft);
            			});
            		}, 1000);
            	} else {
            		setTimeout(function() {
            			$front.toggleClass('hide');
            			$back.toggleClass('hide');
            		}, 320);
            	}
            });
        });
    };
    
    $.fn.flip.defaults = {
    	target: null
    };
})(jQuery);

Raphael.fn.arrow = function (x1, y1, x2, y2, size) {
	var angle = Math.atan2(x1-x2,y2-y1);
	angle = (angle / (2 * Math.PI)) * 360;
	var arrowPath = this.path("M" + x2 + " " + y2 + " L" + (x2  - size) + " " + (y2  - size) + " L" + (x2  - size)  + " " + (y2  + size) + " L" + x2 + " " + y2 ).attr("fill","black").rotate((90+angle),x2,y2);
	var linePath = this.path("M" + x1 + " " + y1 + " L" + x2 + " " + y2);
	return [linePath,arrowPath];
};

$(document).ready(function() {
	
	$('#agree, #disagree').flip({
		target: $('#flipTalkComment')
	});
	
	$('#flipTalkContent .flip-content-icon').flip({
		target: $('#flipTalkContent')
	});
	
	$('.comment').each(function() {
		var $this = $(this);
		
		$this.find('.flip-content-icon').flip({
			target: $this
		});
	});
	
	$('.dial').knob({
		readOnly: true
	});
	
	/*
	var paper = Raphael("paper", 182, 228);
	paper.arrow(0,19,180,38,8);
	*/
});